"""
This is modeling of optoelectronic problems.
"""

import itertools

import numpy as np
from scipy import interpolate

from holmespy.alternativesets import DiscreteAlternativeSet
from holmespy.optimizers import DiscreteOptimizer
from holmespy.state import State


class ConstantNoiseModel():
    """Noise model should be defined as a callable function."""
    
    def __init__(self, sigma2w):
        self.sigma2w = sigma2w
        
    def __call__(self, x):
        return self.sigma2w



class OptoelectronicParams():
    """ 
    This provides required experimental budget to implement
    optoelectronic models.
    """
    
    def __init__(self, min_amt, max_amt, num_amt, min_size, max_size, \
                 num_size, ct_max, time_for_exp, time_for_batch, batch_size, \
                 sigma2W):
        
        assert (min_amt > 0)
        
        self.amt_all = np.linspace(min_amt, max_amt, num_amt).tolist()
        self.size_all = np.linspace(min_size, max_size, num_size).tolist()
        self.x_all = list(itertools.product(self.size_all, self.amt_all))

        self.ct_max = ct_max
        self.time_for_exp = time_for_exp
        self.time_for_batch = time_for_batch
        self.batch_size = batch_size
        
        self.A = DiscreteAlternativeSet(self.x_all)
        
        self.optimizer = DiscreteOptimizer(self.A)
        
        self.sigma2W_f = ConstantNoiseModel(sigma2W)
        
        

class OptoelectronicStateVer1(State):
    """
    Assume we have enough time to run experiments.
    Once we selected one configuration we should run experiment until use them
    all.
    """
    def __init__(self, B, CT, size, amt, params, 
                 hist_actions = None, hist_outcomes = None):
        self.B = B
        self.CT = CT
        self.size = size
        self.amt = amt
        self.params = params
        
        if hist_actions is None:
            self.hist_actions = []
        else:
            self.hist_actions = hist_actions

        if hist_outcomes is None:
            self.hist_outcomes = []
        else:
            self.hist_outcomes = hist_outcomes
            
        # set available actions
        # Case 1: We have enough amount of NPs to use
        if amt >= params.amt_all[0]:
            
            # if we have enough time to run an experiment
            if self.CT + params.time_for_exp < params.ct_max:
                actions_list = [(size, a) \
                    for a in params.amt_all if a < amt]
            # if we don't have time to run an experiment
            else:
                actions_list = []

        # Case 2: We don't have enough amount NPs to use
        else:
            
            # if we have enough time to make a new batch and run an experiment
            if self.CT + params.time_for_exp + params.time_for_batch < \
                params.ct_max:
                actions_list = [x for x in params.x_all]
            # if we don't have time to make a new batch
            else:
                actions_list = []
                    
        self.avail_actions = DiscreteAlternativeSet(actions_list)
        self.current_beliefs = B
            
    def is_terminal(self):
        return len(self.avail_actions) == 0
    
    def available_actions(self):
        return self.avail_actions
    
    def single_period_reward(self, posterior_state):
        return 0
        
    def final_reward(self):
        y, _ = self.params.optimizer.optimize(self.B.mean)
        return y
        
    def step(self, action, outcome):
        posterior = self.B.update(action, outcome, 
                                          self.params.sigma2W_f(action))
        size_p = action[0]
        
        hist_actions = self.hist_actions.copy()
        hist_outcomes = self.hist_outcomes.copy()
        hist_actions.append(action)
        hist_outcomes.append(outcome)
        
        # Case 1: We ran an experiment due to enough amount of NPs in the batch
        if self.amt >= self.params.amt_all[0]:
            amt_p = self.amt - action[1]
            CT_p = self.CT + self.params.time_for_exp
        
        # Case 2: We made a new batch due to lack of NPs
        else:
            amt_p = self.params.batch_size - action[1]
            CT_p = self.CT + self.params.time_for_exp + \
                self.params.time_for_batch
        
        return OptoelectronicStateVer1(posterior, CT_p, size_p, amt_p, 
                                     self.params, hist_actions, hist_outcomes)


class OptoelectronicStateVer2(State):
    """
    Assume we have enough time to run experiments and make a new batch.
    Once one configuration of NPs is synthesized, we can store NPs to use 
    later. Or We can creat a new batch for selected configuration which is 
    lack of amount to run an experiment. 
    """
    
    def __init__(self, B, CT, amt, params, hist_actions = None, 
            hist_outcomes = None):
        self.B = B
        self.CT = CT
        self.amt = amt
        self.params = params
        
        if hist_actions is None:
            self.hist_actions = []
        else:
            self.hist_actions = hist_actions

        if hist_outcomes is None:
            self.hist_outcomes = []
        else:
            self.hist_outcomes = hist_outcomes
            
        # set available actions
        # Case 1: We have time to run an experiment and make a new batch.
        if self.CT + params.time_for_exp + params.time_for_batch <= \
            params.ct_max:
            actions_list = [x for x in params.x_all]
            
        # Case 2: We have time to run an experiment but not make a new batch
        elif (self.CT + params.time_for_exp <= params.ct_max) and \
            (self.CT + params.time_for_exp + params.time_for_batch > \
            params.ct_max):
                
            actions_list = []
            for x in params.x_all:
                k = params.size_all.index(x[0])
                
                if x[1] <= amt[k]:
                    actions_list.append(x)

        # Case 3: We don't have time to run an experiment
        else:
            actions_list = []
        
        self.avail_actions = DiscreteAlternativeSet(actions_list)
        self.current_beliefs = B
                
    def is_terminal(self):
        return len(self.avail_actions) == 0
    
    def available_actions(self):
        return self.avail_actions
    
    def single_period_reward(self, posterior_state):
        return 0
        
    def final_reward(self):
        y, _ = self.params.optimizer.optimize(self.B.mean)
        return y
     
    def step(self, action, outcome):
        posterior = self.B.update(action, outcome, 
                                                self.params.sigma2W_f(action))
        size_p = action[0]
        amt_p = self.amt.copy()
        
        hist_actions = self.hist_actions.copy()
        hist_outcomes = self.hist_outcomes.copy()
        hist_actions.append(action)
        hist_outcomes.append(outcome)
        
        i = self.params.size_all.index(size_p)
        
        # Case 1: We made a new batch for the selected size
        if self.amt[i] < action[1]:
            amt_p[i] = self.params.batch_size - action[1] + self.amt[i]
            CT_p = self.CT + self.params.time_for_exp + \
                self.params.time_for_batch
        
        # case 2: We ran an exmperiment due to enough amount of NPs 
        # for the selected size
        else:
            amt_p[i] = self.amt[i] - action[1]
            CT_p = self.CT + self.params.time_for_exp
            
        return OptoelectronicStateVer2(posterior, CT_p, amt_p, self.params, 
                                   hist_actions, hist_outcomes)
        
        
        
class OptoelectronicAnalyzer():
    
    def __init__(self, params, S0, tnew = None):
        self.params = params
        self.S0 = S0

        # Set the interpolation times
        if(tnew is None):
            self.tnew = np.linspace(0, params.ct_max, 100)
        else:
            self.tnew = tnew
            
    def _calc_ct(self, simulation):
        
        return np.array([0] + 
        [sim_state.state_np1.CT for sim_state in simulation.sim_states])

    def _calc_oc(self, simulation, optimizer):
        max_f_star, _ = optimizer.optimize(simulation.f_star)
    
        oc = np.zeros(len(simulation.sim_states) + 1)
    
        S_n = simulation.params.S_0
        for n in range(len(simulation.sim_states) + 1):
            B_n = S_n.B
            f_n = B_n.mean
            _, i_star = optimizer.optimize(f_n)
            oc[n] = np.abs((max_f_star 
                - simulation.f_star(i_star))/max_f_star)
    
            if(n < len(simulation.sim_states)):
                S_n = simulation.sim_states[n].state_np1
    
        return oc
    
    def oc_transform(self, ct, oc):
        f = interpolate.interp1d(ct, oc, kind = 'previous', 
          fill_value = "extrapolate")
        return f(self.tnew)


