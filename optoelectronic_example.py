"""
This example shows how to set up states and policies, and run simulations
in order to assess the performance of various policies on
two version of optoelectronic problems.
"""
import argparse
import json

from ccr_utils.submit import CCRSubmit, CCRSimulationJob
from holmespy.testfunctions import LookupTablePriorGenerator
from holmespy.policies import EIPolicy, ExplorationPolicy, ExploitationPolicy,\
        MaxVariancePolicy, MCTSPolicy, UCBPolicy
from holmespy.simulator import Simulator, SimulationParameters
from mctsutils.large_scale_simulator import LargeScaleSimulator

import matplotlib.pyplot as plt
import numpy as np

from optoelectronic_models import OptoelectronicParams,\
    OptoelectronicStateVer1, OptoelectronicStateVer2, OptoelectronicAnalyzer



"""
Define parameters
"""

MIN_AMT = .1
MAX_AMT = 1
NUM_AMT = 10
MIN_SIZE = .1
MAX_SIZE = 1
NUM_SIZE = 10
CT_MAX = 1
TIME_FOR_EXP = 0.02
TIME_FOR_BATCH = 0.1

BATCH_SIZE = 3
SIGMA2W = 1**2
COMP_BUDGET = 1024
NUM_SIMS = 200

params = OptoelectronicParams(MIN_AMT, MAX_AMT, NUM_AMT, MIN_SIZE,
                              MAX_SIZE, NUM_SIZE, CT_MAX, TIME_FOR_EXP,
                              TIME_FOR_BATCH, BATCH_SIZE, SIGMA2W)


spec_file = "opto_mcts_sim_spec_1024.txt"

# MCTS default parameters
mcts_params = MCTSPolicy.MCTSParameters(COMP_BUDGET, params.sigma2W_f)


"""
Generate prior beliefs
"""
X = params.A
prior_gen = LookupTablePriorGenerator(X)
B0 = prior_gen.generate_from_kernel(0, 1, 0.2)


"""
Define initial state
Select a version to run
"""

optimizer = params.optimizer
# Version 1
#S0 = OptoelectronicStateVer1(B0, 0, MAX_SIZE, MAX_AMT, params)
# Version 2
S0 = OptoelectronicStateVer2(B0, 0, [0]*NUM_SIZE, params)


"""
Define some policies
"""

ei = EIPolicy(optimizer, params.sigma2W_f)
xplr = ExplorationPolicy()
xplt = ExploitationPolicy(optimizer)
maxv = MaxVariancePolicy(optimizer)
ucbp = UCBPolicy(optimizer)
mcts = MCTSPolicy(mcts_params)

policies = [ei, xplr, xplt, maxv, ucbp, mcts]
policy_labels = ["EI", "Exploration", "Exploitation", "Max Variance", "UCB",
                 "MCTS"]


"""
Sets up and runs the large-scale simulator for the Optoelectronic problem
"""


def run_large_scale_simulator(spec_file, local_base_dir=".",
        submit = False, retrieve = False, analyze = False, time = False,
        local = False):

    """
    Performs actions corresponding to the sequence of simulations in the
    simulation spec file.
    """

    # Load simulation spec
    with open(spec_file) as f:
        kwargs = json.load(f)
    kwargs['local_base_dir'] = local_base_dir

    # Problem specific params
    kwargs['X'] = params.A
    kwargs['optimizer'] = params.optimizer
    kwargs['sigma2W_f'] = params.sigma2W_f
    kwargs['S0'] = S0

    tnew = np.linspace(0, CT_MAX, 100)
    analyzer = OptoelectronicAnalyzer(params, S0, tnew=tnew)
    kwargs['oc_transform'] = analyzer.oc_transform
    kwargs['oc_N'] = analyzer.tnew
    #
    # Instantiate simulator
    simulator = LargeScaleSimulator(**kwargs)

    # Perform the requested action
    if(submit):
        simulator.submit_all_simulations(local = local)

    if(retrieve):
        simulator.retrieve_results()

    if(analyze):
        simulator.calc_oc(parallel=False)
        simulator.plot_oc()
        simulator.plot_oc_samples()
        simulator.print_best_learning_rate()

    if(time):
        simulator.time_policies()


run_large_scale_simulator(spec_file, local_base_dir=".",
        submit =False, retrieve = True, analyze = False, time = False,
        local = False)



"""
Set-up and run simulations
"""

# simulator = Simulator(NUM_SIMS, B0, verbose=True)
# all_sim_res = []
#
# # Run simulations over different policies
# # for (i,pi) in enumerate(policies):
# #    print("Policy: " + policy_labels[i])
# #    sim_params = SimulationParameters(S0, pi, params.sigma2W_f)
# #    sim_res = simulator.simulate(sim_params)
# #    all_sim_res.append(sim_res)
#
# Run large scale simulations by CCR
# ccr_submit = CCRSubmit("soojungb", "/Users/soojung/.ssh/id_rsa",
#                        "vortex.ccr.buffalo.edu")
# for (i,pi) in enumerate(policies):
#     print("Policy: " + policy_labels[i])
#     sim_params = SimulationParameters(S0, pi, params.sigma2W_f)
#     ccr_job = CCRSimulationJob("git@bitbucket.org:csmslab/optoelectronic.git",
#                                "master", "src",
                               "/projects/academic/kreyes3/projects/mcts/sims",
#                                "/projects/academic/kreyes3/environments/holmespy-env/env",
#                                simulator, sim_params, job_name="opto_sim",
#                                num_jobs=20, job_time="03:30:00")
#     ccr_submit.setup(ccr_job)
#     ccr_submit.submit(ccr_job, wait=True)
#     sim_res = ccr_submit.retrieve_results(ccr_job)
#     all_sim_res.append(sim_res)

#
# """
# Analyze simulation results using OC plots
# """
#
#
# # # plot truths
# #    for l, f in enumerate(simulator.f_truths):
# #        Z = np.zeros([NUM_SIZE, NUM_AMT])
# #
# #        for x in params.x_all:
# #            k = params.size_all.index(x[0])
# #            j = params.amt_all.index(x[1])
# #            Z[k,j] = f(x)
# #        plt.contourf(params.amt_all, params.size_all, Z)
# #        plt.colorbar()
# #
# #        states = sim_res[l].sim_states
# #        selected_sizes = np.zeros(len(states))
# #        selected_amounts = np.zeros(len(states))
# #
# #        for j, state in enumerate(states):
# #            x = state.a_n
# #            selected_sizes[j] = x[0]
# #            selected_amounts[j] = x[1]
# #            plt.text(selected_amounts[j]-0.015, selected_sizes[j]-0.015, str(j),
# #                     size=12, color='white', weight='bold')
# #            plt.scatter(selected_amounts[j], selected_sizes[j], s=150, \
# #                        c="crimson")
# #
# #        plt.xlabel("Amount", size = 12)
# #        plt.ylabel("Size", size = 12)
# #        plt.savefig(str(policy_labels[i]) +'_truth_'+ str(l) + '.pdf')
# #        plt.show()
#
#
# # calculate OC for all policies, simulations and experiments
# analyzer = OptoelectronicAnalyzer(params, S0)
# tnew = np.linspace(0, CT_MAX, 100)
# OC = np.zeros([len(policies), NUM_SIMS, len(tnew)])
#
# for i, sim_res in enumerate(all_sim_res):
#    new_batch_all = []
#    exp_all = []
#
#    for j, simulation in enumerate(sim_res):
#        OC[i,j,:] = analyzer.oc_transform(
#                analyzer._calc_ct(simulation),
#                analyzer._calc_oc(simulation, optimizer))
#        list_of_ct = analyzer._calc_ct(simulation)
#
#    # count how many new batches generated
#        num_new_batch = 0
#        num_exp = 0
#        for k in range(len(list_of_ct)):
#              if (list_of_ct[k] == list_of_ct[k-1] + TIME_FOR_BATCH +
#                 TIME_FOR_EXP):
#                  num_new_batch += 1
#              else:
#                  num_exp += 1
#
#        new_batch_all.append(num_new_batch)
#        exp_all.append(num_exp + num_new_batch)
#    avg_num_batch = np.mean(new_batch_all)
#    avg_num_exp = np.mean(exp_all)
#
#    median_oc = np.median(OC, axis = 1)
#    lo_oc = np.percentile(OC, 33, axis = 1)
#    hi_oc = np.percentile(OC, 66, axis = 1)
#
#    plt.suptitle("Avg number of new batches generated: " + str(avg_num_batch) + "\n" +
#                 "Avg number of experiment: " + str(avg_num_exp))
#
#    plt.ylim((-0.1, 1.3))
#
#    # plot each OC plot with error bars
#    plt.plot(tnew, median_oc[i], linewidth = 3.0, color = 'navy')
#    plt.legend(policy_labels[i:])
#    plt.fill_between(tnew, lo_oc[i,:], hi_oc[i,:], color = 'k', alpha = 0.1)
#
#    plt.grid()
# #   plt.savefig('ver2_mcts' + str(policy_labels[i]) + '.pdf')
#    plt.show()
#
# ##     plot all the OC curves in one figure
# #    plt.plot(tnew, median_oc[i], linewidth = 3.0)
# #    plt.legend(policy_labels)
# # plt.grid()
# # plt.savefig('FILE_NAME.pdf')
# # plt.show()
#
